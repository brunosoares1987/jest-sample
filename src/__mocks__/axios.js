export default { 
    // Sugar syntax, both codes have the same result.   
    // get: jest.fn(() => Promise.resolve({ data: {} }))
    get: jest.fn(() => jest.fn().mockResolvedValue()),
    post: jest.fn(() => jest.fn().mockResolvedValue())
  }