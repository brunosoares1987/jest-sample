import Math from '../../src/calculator/math'
import Logger from '../../src/calculator/logger'

// Sample class to perform some unit tests.
export default class Calculator {
    constructor() {
        this.math = new Math()
        this.logger = new Logger()        
    }

    sum(a, b) {        
        // Just a simple console log code to simulate something that needs to be mocked.
        // but doesn´t return anything.
        this.logger.logMessage('calculator was called')

        // Simple logic to create a different scenario on Calculator unit test.
        if(a && b) {
            let result = this.math.sum(a, b)
            return result
        }      

         // Just to have an easy code coverage example
         if(a == 4900)
         return 'no coverage'

        return 0       
    }
}