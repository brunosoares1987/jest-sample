import axios from "axios";

module.exports = class TestApi {
    // Just a simple get using an external module.
    // There is a unit test to see how to mock an module.
    async getSomeData () {        
        let result = await axios.get('https://api.github.com/users/test')     
        
        console.log(result.data)
        console.log(result.status)

        return result
    }
}
