To run an specific test:
npm test -- -u -t="put the test name here"

To watch the unit test files, add the parameter:
--watch 

more examples of command line https://jestjs.io/docs/en/cli#testnamepattern-regex

To debug the project use the launch.json config file, more details about the debug.
https://jestjs.io/docs/en/troubleshooting#debugging-in-vs-code


About execution order:
https://jestjs.io/docs/en/setup-teardown#scoping

Next steps:

- Static mocks
- Freeze time
- Create a test to define standards

Autocomplete:
npm install --save @types/jest

Watch function:
Can be used executing an command on npm or using this extension https://github.com/jest-community/vscode-jest