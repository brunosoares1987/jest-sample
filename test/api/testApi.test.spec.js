import TestApi  from '../../src/api/testApi'

/* Notice here that I´m importing the Axios but not the real implementation
 Check the src folder src\__mocks__\axios.js
 The modules mock should be done this way
 First create a file with the same name like the module that you want to mock
 There you need to create a kind of a mock of the method that you need but without a return value
 the return value will be defined here in the test scenario because it is something the
 changes based on scenarios.
 */
import mockAxios from "axios"

// The purpose of this test is to show how to mock a Module.
describe('How to do a module mock.', () => {
    /* Notice that we are testing a Axios Get call
    which is an async method to we need to use the async keyword here
    */
    it('Test a module mock.', async () => {
        // Arrange  
        let apiResult = {
            data: { results: ["test.jpg"] },
            status: 200
        }

        // Because get is an async method the way that we mock it
        // is using a different jest method
        mockAxios.get.mockResolvedValue(apiResult)   

        let testApi = new TestApi();
        
        // Act
        let result = await testApi.getSomeData()
        
        // Assert
        expect(result.status).toBe(200)
        expect(result.data.results).toBe["test.jpg"]
    })
})