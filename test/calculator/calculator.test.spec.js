/* Import all the dependencies that you need
    include here the class that you want to test
    and the classes that you need to mock.
*/
import Calculator from '../../src/calculator/calculator'
import Math from '../../src/calculator/math'
import Logger from '../../src/calculator/logger'

/*  This is the first step to mock a class.
    Just adding the class here will 
    replace the original implementation 
    by an automatic mock.
*/ 
jest.mock('../../src/calculator/math')
jest.mock('../../src/calculator/logger') 

/*  Use the describe block to group tests
    This way you keep the tests for a same method grouped in
    one place, make the test readable and organized.
*/
describe('Calculator unit tests.', () => {    

    beforeAll(() => {      
       
    })

    /*  Always use a beforeEach block
        here is the place that you clear some stuff
        like mocks between the tests
        so if you have 3 tests inside 1 describe
        this block will run every time before a test start.
        This "clear" step is important to make your tests run without
        conflicts with other tests. 
    */
    beforeEach(() => {
        Logger.mockClear() // Only clear the calls like how many calls the mocked method have.
        Math.mockReset() // Has the same behavior like mock clear plus clear the mocked implementation.   
        
        // There are scenarios below to show the usage of this block and the methods inside it.
    });  

    it('It will check how many times the mocked method was called.', () => {     
        /*  To make the test cases readable
            create sections to organize the code
            Setup Or Arrange - Will be used to create all the stuff that
            you method needs to run. Choose between one of the terms.
            Act - Will call the method that you want to unit test
            Assert - The place used to test the returned value or 
            some other value(like here number of calls) to check if
            everything is working as expected.
        */
        
        // Setup Or Arrange
        const sum = jest.fn();
        Math.mockImplementation(() => {
            return { sum }            
        })

        const calculator = new Calculator();
        
        // Act
        calculator.sum(1, 1)

        // Assert
        // Take care using this kind of assert      
        expect(sum).toHaveBeenCalledTimes(1);   
    });

    it('It will show how to mock a method return value.', () => {
        /* it is important to respect the precedence otherwise the mock won´t work
            so if you instantiate a class that has dependecies that you are planning to mock
            you need to mock before so the instance that you will get will have the reference to mock in the memory            
        */ 
        // TODO how it would work with method overload? it seems that it is not mandatory to use parameters.

        //Arrange or Setup
        Math.mockImplementation(() => {
            return {                               
                    sum: () => {
                        return 66
                    }
            };
        });

        const calculator = new Calculator()  

        /*  Notice that doesn´t matter the parameters that you pass 
            the value returned by the method is defined above in the mock.
        */
        //Act
        const result = calculator.sum(1, 2)

        // Assert
        expect(result).toBe(66)
    })

    /* This test show what happens if you forget to reset your mock
        if you don´t reset the mock, the method return values that you mocked will continue existing
        comment "Math.mockReset()" on "beforeEach section" and run this test.
    */
    it('Show what happens if you don´t reset a mock.', () => {
        // Arrange     
        const calculator = new Calculator()  

        // Act
        let result = calculator.sum(1, 2)

        /* The returned value should be undefined
            because in theory here I only have the automatic mock
            jest.mock('../../src/calculator/math') which replaces
            the methods implementation and always returns undefined
            unless I define a different value.
            But the return that you will get is a number
            because the mock returned value from other test wasn´t cleared
            before the test execution.
        */
        // Assert
        expect(result).toBe(undefined)
    })

    it('Show how mockClear works.', () => {  
        // Act
        /* There is no act here so you
        can see that the sum mocked method is not being called
        so it shouldn´t have calls on expect.
        */    

        // Assert
        /* uncomment the Logger.mockClear() on beforeEach section
            so oyu can check what happens if you don´t clear the mock.
        */
        expect(Logger).not.toHaveBeenCalled()
    });  

    /* The purpose of this test is show how to use a table of inputs
    so if you have a lot of flows based on input values
    it will help you to write just 1 unit test instead of one unit per flow.
    */
    test.each([
        [1, 1, 2],
        [1, 2, 3],
        [1, null, 0],
    ])('.add(%i, %i)', (a, b, expected) => {  
        // Arrange      
        Math.mockImplementation(() => {
            return {                           
                sum: () => {                
                    return expected
                }
            }
        })
        const calculator = new Calculator()  

        // Act
        let result = calculator.sum(a, b)

        // Assert
        console.log('the sum of a ' + a + ' + ' + b + ' is ' + expected )
        expect(result).toBe(expected)    
    })

    it('Show how to mock a returned value once.', () => {
        // Arrange
        const sumMock = jest
            .fn()
            .mockImplementationOnce(() => 66)
            .mockImplementationOnce(() => 65)
    
        Math.mockImplementation(() => {
            return { sum: sumMock }            
        });
    
        const calculator = new Calculator()  
    
        // Act 
        let result = calculator.sum(1, 2)
    
        // Assert
        expect(result).toBe(66)
    
        // Act
        result = calculator.sum(1, 2)
    
        // Assert
        expect(result).toBe(65)
    })
})